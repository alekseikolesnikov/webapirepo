﻿using System;

namespace Services.DTO
{
    public class CommentDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }

        public int CardId { get; set; }
        public int UserId { get; set; }
    }
}
