﻿namespace Services.DTO
{
    public class UserDto
    {
        private string _password;
        public int Id { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get => _password.GetHashCode().ToString(); set => _password = value; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
