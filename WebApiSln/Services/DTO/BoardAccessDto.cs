﻿using System;

namespace Services.DTO
{
    public class BoardAccessDto
    {
        public Guid Id { get; set; }

        public int PermissionId { get; set; }
        public int UserId { get; set; }
        public int BoardId { get; set; }
    }
}
