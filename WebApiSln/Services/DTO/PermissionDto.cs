﻿namespace Services.DTO
{
    public class PermissionDto
    {
        public int Id { get; set; }
        public string Role { get; set; }
    }
}
