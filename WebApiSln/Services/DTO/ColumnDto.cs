﻿namespace Services.DTO
{
    public class ColumnDto
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public int BoardId { get; set; }
    }
}
