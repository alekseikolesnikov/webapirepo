﻿using AutoMapper;
using DataBaseLib.Models;
using FluentValidation;
using RepositoryLib.Interfaces;
using Services.DTO;
using Services.Interfaces;
using System.Diagnostics;

namespace Services
{
    public class BoardService : IBoardService
    {
        private readonly IMapper _mapper;
        private readonly IValidator<BoardDto> _validator;
        private readonly IGenericRepository<Board> _boards;
        public BoardService(IMapper mapper, IValidator<BoardDto> validator, IGenericRepository<Board> boards)
        {
            _mapper = mapper;
            _validator = validator;
            _boards = boards;
        }
        public BoardDto Create(BoardDto dto)
        {
            Validate(dto);
            var board = _mapper.Map<Board>(dto);
            _boards.Create(board);

            return _mapper.Map<BoardDto>(board);
        }
        public BoardDto Read(int id)
        {
            var board = _boards.Read(id);
            var dto = _mapper.Map<Board, BoardDto>(board);

            return dto;
        }
        public BoardDto Update(BoardDto dto)
        {
            Validate(dto);
            var updated = _mapper.Map<BoardDto, Board>(dto);
            _boards.Update(updated);

            return _mapper.Map<BoardDto>(updated);
        }
        public void Delete(BoardDto dto)
        {
            var board = _mapper.Map<BoardDto, Board>(dto);
            _boards.Delete(board);
        }

        public void Validate(BoardDto dto)
        {
            var fact = _validator.Validate(dto);
            if (!fact.IsValid)
            {
                foreach(var error in fact.Errors)
                    Debug.WriteLine(error.ErrorMessage);
            }
        }

        //public void Create(BoardDto dto)
        //{
        //    _boards.Create(new Board() { Title = dto.Title, Description = dto.Description });
        //}

        //public BoardDto Read(int id)
        //{
        //    var board = _boards.Read(id);

        //    var dto = new BoardDto()
        //    {
        //        Id = board.Id,
        //        Description = board.Description,
        //        Title = board.Title,
        //    };

        //    return dto;
        //}

        //public void Update(BoardDto dto)
        //{
        //    var board = _boards.Read(dto.Id);
        //    board.Title = dto.Title;
        //    board.Description = dto.Description;

        //    _boards.Update(board);
        //}

        //public void Delete(BoardDto dto)
        //{
        //    var board = _boards.Read(dto.Id);
        //    _boards.Delete(board);
        //}
    }
}
