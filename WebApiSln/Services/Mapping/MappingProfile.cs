﻿using AutoMapper;
using DataBaseLib.Models;
using Services.DTO;

namespace Services.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Board, BoardDto>().ReverseMap();
            CreateMap<Card, CardDto>().ReverseMap();
        }
    }
}
