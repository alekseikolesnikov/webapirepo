﻿using Services.DTO;

namespace Services.Interfaces
{
    public interface ICardService
    {
        public CardDto Create(CardDto entity, int columnId);
        public CardDto Read(int id);
        public CardDto Update(CardDto entity);
        public void Delete(CardDto entity);
    }
}
