﻿using Services.DTO;

namespace Services.Interfaces
{
    public interface IBoardService
    {
        public BoardDto Create(BoardDto entity);
        public BoardDto Read(int id);
        public BoardDto Update(BoardDto entity);
        public void Delete(BoardDto entity);
    }
}
