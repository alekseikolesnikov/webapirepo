﻿using AutoMapper;
using DataBaseLib.Models;
using FluentValidation;
using RepositoryLib.Interfaces;
using Services.DTO;
using Services.Interfaces;
using System.Diagnostics;

namespace Services
{
    public class CardService : ICardService
    {
        private readonly IMapper _mapper;
        private readonly IValidator<CardDto> _validator;
        private readonly IGenericRepository<Card> _cards;

        public CardService(IMapper mapper, IValidator<CardDto> validator, IGenericRepository<Card> cards)
        {
            _mapper = mapper;
            _validator = validator;
            _cards = cards;
        }

        public CardDto Create(CardDto dto, int columnId)
        {
            Validate(dto);
            var card = _mapper.Map<Card>(dto);
            card.ColumnId = columnId;
            _cards.Create(card);

            return _mapper.Map<CardDto>(card);
        }

        public CardDto Read(int id)
        {
            var card = _cards.Read(id);
            var dto = _mapper.Map<Card, CardDto>(card);

            return dto;
        }

        public CardDto Update(CardDto dto)
        {
            Validate(dto);
            var card = _mapper.Map<CardDto, Card>(dto);
            _cards.Update(card);

            return _mapper.Map<CardDto>(card);
        }

        public void Delete(CardDto dto)
        {
            var card = _mapper.Map<CardDto, Card>(dto);
            _cards.Delete(card);
        }

        public void Validate(CardDto dto)
        {
            var fact = _validator.Validate(dto);
            if (!fact.IsValid)
            {
                foreach (var error in fact.Errors)
                    Debug.WriteLine(error.ErrorMessage);
            }
        }
    }
}
