﻿using FluentValidation;
using Services.DTO;

namespace Services.Validators
{
    public class CardValidator : AbstractValidator<CardDto>
    {
        public CardValidator()
        {
            RuleFor(c => c.Id).NotNull().GreaterThan(0);
            RuleFor(c => c.Title).NotNull().MaximumLength(100);
            RuleFor(c => c.Description).NotNull().MaximumLength(450);
        }
    }
}
