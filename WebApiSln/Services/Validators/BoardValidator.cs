﻿using FluentValidation;
using Services.DTO;

namespace Services.Validators
{
    public class BoardValidator : AbstractValidator<BoardDto>
    {
        public BoardValidator()
        {
            RuleFor(b => b.Title).NotNull();
            RuleFor(b => b.Title).NotNull().MaximumLength(100);
            RuleFor(b => b.Description).NotNull().MaximumLength(450);
        }
    }
}
