﻿namespace RepositoryLib.Interfaces
{
    public interface IGenericRepository<T> where T : class
    {
        public T Create(T entity);
        public T Read(int id);
        public T Update(T entity);
        public T Delete(T entity);
    }
}
