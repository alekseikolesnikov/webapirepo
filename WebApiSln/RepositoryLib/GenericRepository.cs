﻿using DataBaseLib;
using Microsoft.EntityFrameworkCore;
using RepositoryLib.Interfaces;

namespace RepositoryLib
{
    public class GenericRepository<T> : IGenericRepository<T>
        where T : class
    {
        private readonly DataContext _context;
        public GenericRepository(DataContext context)
        {
            _context = context;
        }

        public T Create(T entity)
        {
            _context.Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public T Delete(T entity)
        {
            _context.Remove(entity);
            _context.SaveChanges();
            return entity;
        }

        public T Read(int id)
        {
            return _context.Find<T>(id);
        }

        public T Update(T entity)
        {
            var result = _context.Update(entity).Entity;
            _context.SaveChanges();

            return result;
        }
    }
}
