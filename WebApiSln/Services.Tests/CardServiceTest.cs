﻿using AutoMapper;
using DataBaseLib.Models;
using FluentValidation;
using Moq;
using RepositoryLib.Interfaces;
using Services.DTO;
using Services.Interfaces;
using Services.Mapping;
using Services.Validators;
using Xunit;

namespace Services.Tests
{
    public class CardServiceTest
    {
        private readonly ICardService _sut;

        private readonly IMapper _mapper = new MapperConfiguration(config => config.AddProfile(new MappingProfile())).CreateMapper();
        private readonly IValidator<CardDto> _validator = new CardValidator();
        private readonly Mock<IGenericRepository<Card>> _repo = new Mock<IGenericRepository<Card>>();
        public CardServiceTest()
        {
            _sut = new CardService(_mapper, _validator, _repo.Object);
        }

        [Fact]
        public void Create_ShouldCreateNewCarWithParameters_ReturnsNewCard()
        {
            var expected = new Card()
            {
                Id = 1,
                ColumnId = 1,
                Title = "Test title",
                Description = "Test Description",
            };

            _repo
                .Setup(r => r.Create(expected))
                .Returns(expected);

            var actual = _sut.Create(new CardDto()
            {
                Id = 1,
                Title = "Test title",
                Description = "Test Description",
            }, 1);


            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Title, actual.Title);
            Assert.Equal(expected.Description, actual.Description);
            Assert.Equal(expected.ColumnId, actual.ColumnId);
        }
    }
}
