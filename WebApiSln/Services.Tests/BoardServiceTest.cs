using AutoMapper;
using DataBaseLib.Models;
using FluentValidation;
using Moq;
using RepositoryLib.Interfaces;
using Services.DTO;
using Services.Interfaces;
using Services.Mapping;
using Services.Tests.TestData;
using Services.Validators;
using Xunit;

namespace Services.Tests
{
    public class BoardServiceTest
    {
        private readonly IBoardService _sut;

        private readonly IMapper _mapper = new MapperConfiguration(config => config.AddProfile(new MappingProfile())).CreateMapper();
        private readonly IValidator<BoardDto> _validator = new BoardValidator();
        private readonly Mock<IGenericRepository<Board>> _repo = new Mock<IGenericRepository<Board>>();

        public BoardServiceTest()
        {
            _sut = new BoardService(_mapper, _validator, _repo.Object);
        }

        [Theory]
        [ClassData(typeof(BoardTestData))]
        public void Read_ShouldReturnBoard_WhenBoardExists(int id, string title, string description)
        {
            var expected = new Board
            {
                Id = id,
                Title = title,
                Description = description,
            };

            _repo
                .Setup(r => r.Read(id))
                .Returns(expected);


            var actual = _sut.Read(id);

            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Title, actual.Title);
            Assert.Equal(expected.Description, actual.Description);
        }
    }
}
