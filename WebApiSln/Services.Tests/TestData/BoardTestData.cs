﻿using System.Collections;
using System.Collections.Generic;

namespace Services.Tests.TestData
{
    public class BoardTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { 1, "Test Title", "Test Description" };
            yield return new object[] { 2, "Second Title", "Second Description" };
            yield return new object[] { 3, "Third Title", "Third Description" };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
