﻿using DataBaseLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace DataBaseLib.ModelsConfigurations
{
    public class CommentConfig : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Date).IsRequired().HasDefaultValue(DateTime.Now);
            builder.Property(c => c.Text).IsRequired().HasMaxLength(450);
        }
    }
}
