﻿using DataBaseLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBaseLib.ModelsConfigurations
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(u => u.Id);

            //builder.HasAlternateKey(u => u.Login);
            builder.HasIndex(u => u.Login).IsUnique().HasDatabaseName("IX_Login");
            builder.HasAlternateKey(u => new { u.FirstName, u.LastName });

            builder.Ignore(u => u.FullName);
            builder.Property(u => u.FullName).HasComputedColumnSql("[FirstName] + ' ' + [LastName]");

            builder.Property(u => u.Email).IsRequired().HasMaxLength(30);
            builder.Property(u => u.Login).IsRequired().HasMaxLength(30);
            builder.Property(u => u.Password).IsRequired().HasMaxLength(30);
            builder.Property(u => u.FirstName).IsRequired();
            builder.Property(u => u.LastName).IsRequired();

            builder.HasMany(u => u.BoardAccesses).WithOne(ba => ba.User).HasForeignKey(ba => ba.UserId);
        }
    }
}

