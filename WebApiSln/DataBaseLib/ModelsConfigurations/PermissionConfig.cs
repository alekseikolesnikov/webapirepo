﻿using DataBaseLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBaseLib.ModelsConfigurations
{
    public class PermissionConfig : IEntityTypeConfiguration<Permission>
    {
        public void Configure(EntityTypeBuilder<Permission> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Role).IsRequired().HasMaxLength(50);

            builder.HasMany(p => p.BoardAccesses).WithOne(ba => ba.Permission).HasForeignKey(ba => ba.PermissionId);
        }
    }
}
