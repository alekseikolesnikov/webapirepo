﻿using DataBaseLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBaseLib.ModelsConfigurations
{
    public class BoardAccessConfig : IEntityTypeConfiguration<BoardAccess>
    {
        public void Configure(EntityTypeBuilder<BoardAccess> builder)
        {
            //builder.HasNoKey();
            builder.HasAlternateKey(ba => ba.Id);

            builder.HasOne(ba => ba.Board).WithMany(b => b.BoardAccesses).HasForeignKey(ba => ba.BoardId);
            builder.HasOne(ba => ba.User).WithMany(u => u.BoardAccesses).HasForeignKey(ba => ba.UserId);
            builder.HasOne(ba => ba.Permission).WithMany(u => u.BoardAccesses).HasForeignKey(ba => ba.PermissionId);
        }
    }
}
