﻿using System.Collections.Generic;

namespace DataBaseLib.Models
{
    public class User
    {
        private string _password;
        public int Id { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get => _password.GetHashCode().ToString(); set => _password = value; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual List<Card> Cards { get; set; } = new List<Card>();
        public virtual List<Comment> Comments { get; set; } = new List<Comment>();
        public virtual List<BoardAccess> BoardAccesses { get; set; } = new List<BoardAccess>();
    }
}
