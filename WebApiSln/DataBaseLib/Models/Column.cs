﻿using System.Collections.Generic;

namespace DataBaseLib.Models
{
    public class Column
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public int BoardId { get; set; }
        public virtual Board Board { get; set; }

        public virtual List<Card> Cards { get; set; } = new List<Card>();
    }
}
