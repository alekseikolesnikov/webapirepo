﻿using System.Collections.Generic;

namespace DataBaseLib.Models
{
    public class Permission
    {
        public int Id { get; set; }
        public string Role { get; set; }
        public virtual List<BoardAccess> BoardAccesses { get; set; } = new List<BoardAccess>();
    }
}
