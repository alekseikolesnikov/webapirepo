﻿using System.Collections.Generic;

namespace DataBaseLib.Models
{
    public class Card
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public int ColumnId { get; set; }
        public virtual Column Column { get; set; }

        public virtual List<User> Users { get; set; } = new List<User>();
        public virtual List<Label> Labels { get; set; } = new List<Label>();
        public virtual List<Comment> Comments { get; set; } = new List<Comment>();
    }
}
