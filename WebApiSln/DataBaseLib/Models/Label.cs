﻿using System.Collections.Generic;

namespace DataBaseLib.Models
{
    public class Label
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<Card> Cards { get; set; } = new List<Card>();
    }
}
