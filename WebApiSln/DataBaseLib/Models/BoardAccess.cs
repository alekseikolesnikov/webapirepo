﻿using System;

namespace DataBaseLib.Models
{
    public class BoardAccess
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public int PermissionId { get; set; }
        public virtual Permission Permission { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int BoardId { get; set; }
        public virtual Board Board { get; set; }
    }
}
