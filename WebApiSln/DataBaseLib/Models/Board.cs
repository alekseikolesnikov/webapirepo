﻿using System.Collections.Generic;

namespace DataBaseLib.Models
{
    public class Board
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public virtual List<Column> Columns { get; set; } = new List<Column>();
        public virtual List<BoardAccess> BoardAccesses { get; set; } = new List<BoardAccess>();
    }
}
