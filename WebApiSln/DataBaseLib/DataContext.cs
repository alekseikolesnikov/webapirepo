﻿using DataBaseLib.Models;
using DataBaseLib.ModelsConfigurations;
using DataBaseLib.ModelsSeeds;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace DataBaseLib
{
    public class DataContext : DbContext
    {
        private static IConfigurationRoot config = new ConfigurationBuilder()
                                                        .SetBasePath(Directory.GetCurrentDirectory())
                                                        .AddJsonFile("appsettings.json")
                                                        .Build();

        private static string connectionString = config.GetConnectionString("DefaultConnection");
        public DbSet<Board> Boards { get; set; }
        public DbSet<BoardAccess> BoardAccesses { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Column> Columns { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Label> Labels { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<User> Users { get; set; }

        public DataContext() { }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
            //Database.Migrate();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLazyLoadingProxies()
                .UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BoardAccessConfig());
            modelBuilder.ApplyConfiguration(new BoardConfig());
            modelBuilder.ApplyConfiguration(new CardConfig());
            modelBuilder.ApplyConfiguration(new ColumnConfig());
            modelBuilder.ApplyConfiguration(new CommentConfig());
            modelBuilder.ApplyConfiguration(new LabelConfig());
            modelBuilder.ApplyConfiguration(new PermissionConfig());
            modelBuilder.ApplyConfiguration(new UserConfig());

            modelBuilder.ApplyConfiguration(new BoardAccessSeed());
            modelBuilder.ApplyConfiguration(new BoardSeed());
            modelBuilder.ApplyConfiguration(new CardSeed());
            modelBuilder.ApplyConfiguration(new ColumnSeed());
            modelBuilder.ApplyConfiguration(new CommentSeed());
            modelBuilder.ApplyConfiguration(new LabelSeed());
            modelBuilder.ApplyConfiguration(new PermissionSeed());
            modelBuilder.ApplyConfiguration(new UserSeed());
        }
    }
}
