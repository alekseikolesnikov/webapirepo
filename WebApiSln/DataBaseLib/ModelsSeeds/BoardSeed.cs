﻿using DataBaseLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBaseLib.ModelsSeeds
{
    public class BoardSeed : IEntityTypeConfiguration<Board>
    {
        public void Configure(EntityTypeBuilder<Board> builder)
        {
            builder.HasData(
                new Board[]
                {
                    new Board {Id = 1, Title = "Perfect board."},
                    new Board {Id = 2, Title = "Perfect board2."},
                });
        }
    }
}
