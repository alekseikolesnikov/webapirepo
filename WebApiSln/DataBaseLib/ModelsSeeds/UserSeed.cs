﻿using DataBaseLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBaseLib.ModelsSeeds
{
    public class UserSeed : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasData(
                new User[]
                {
                    new User {Id = 1, Email = "admin@gamil.com", FirstName ="Boris", LastName = "Moiseev", Login = "borya", Password = "borya123456"},
                    new User {Id = 2, Email = "user1@gamil.com", FirstName ="Gennadiy", LastName = "Lepestkov", Login = "genka", Password = "aezakmi"},
                    new User {Id = 3, Email = "user2@gamil.com", FirstName ="Vasiliy", LastName = "Rogov", Login = "rogovoi", Password = "rogX221!KDs"},
                });
        }
    }
}

