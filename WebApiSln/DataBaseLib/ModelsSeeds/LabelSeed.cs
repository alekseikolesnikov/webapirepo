﻿using DataBaseLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBaseLib.ModelsSeeds
{
    public class LabelSeed : IEntityTypeConfiguration<Label>
    {
        public void Configure(EntityTypeBuilder<Label> builder)
        {
            builder.HasData(
                new Label[]
                {
                    new Label{ Id = 1, Name = "Label1"},
                    new Label{ Id = 2, Name = "Label2"},
                });
        }
    }
}
