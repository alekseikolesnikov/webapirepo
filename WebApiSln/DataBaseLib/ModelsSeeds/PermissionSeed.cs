﻿using DataBaseLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBaseLib.ModelsSeeds
{
    public class PermissionSeed : IEntityTypeConfiguration<Permission>
    {
        public void Configure(EntityTypeBuilder<Permission> builder)
        {
            builder.HasData(
                new Permission[]
                {
                    new Permission{ Id = 1, Role = "Owner"},
                    new Permission{ Id = 2, Role = "Commentator"},
                    new Permission{ Id = 3, Role = "Collaborator"},
                });
        }
    }
}
