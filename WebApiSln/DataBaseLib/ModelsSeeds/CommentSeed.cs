﻿using DataBaseLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBaseLib.ModelsSeeds
{
    public class CommentSeed : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.HasData(
                new Comment[]
                {
                    new Comment{ Id = 1, Text = "lol", UserId = 1, CardId = 1},
                    new Comment{ Id = 2, Text = "lamo", UserId = 2, CardId = 1},
                });
        }
    }
}