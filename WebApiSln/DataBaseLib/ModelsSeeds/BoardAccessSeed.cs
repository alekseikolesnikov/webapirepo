﻿using DataBaseLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBaseLib.ModelsSeeds
{
    public class BoardAccessSeed : IEntityTypeConfiguration<BoardAccess>
    {
        public void Configure(EntityTypeBuilder<BoardAccess> builder)
        {
            builder.HasData(
                new BoardAccess[]
                {
                    new BoardAccess{ UserId = 1, BoardId = 1, PermissionId = 1},
                    new BoardAccess{ UserId = 2, BoardId = 1, PermissionId = 2},
                    new BoardAccess{ UserId = 1, BoardId = 2, PermissionId = 2},
                    new BoardAccess{ UserId = 2, BoardId = 2, PermissionId = 1},
                    new BoardAccess{ UserId = 3, BoardId = 2, PermissionId = 3},
                });
        }
    }
}