﻿using DataBaseLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBaseLib.ModelsSeeds
{
    public class CardSeed : IEntityTypeConfiguration<Card>
    {
        public void Configure(EntityTypeBuilder<Card> builder)
        {
            builder.HasData(
                new Card[]
                {
                    new Card {Id = 1, Title = "My card", ColumnId = 1},
                    new Card {Id = 2, Title = "My card2", ColumnId = 1},
                    new Card {Id = 3, Title = "My card3", ColumnId = 2},
                });
        }
    }
}