﻿using DataBaseLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBaseLib.ModelsSeeds
{
    public class ColumnSeed : IEntityTypeConfiguration<Column>
    {
        public void Configure(EntityTypeBuilder<Column> builder)
        {
            builder.HasData(
                new Column[]
                {
                    new Column {Id = 1, Title = "Column1", BoardId = 1},
                    new Column {Id = 2, Title = "Column2", BoardId = 2},
                    new Column {Id = 3, Title = "Column2", BoardId = 1},
                });
        }
    }
}
