using AutoMapper;
using DataBaseLib;
using DataBaseLib.Models;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using RepositoryLib;
using RepositoryLib.Interfaces;
using Services;
using Services.DTO;
using Services.Interfaces;
using Services.Mapping;
using Services.Validators;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            //services.AddDbContext<DbContext, DataContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddDbContext<DataContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("WebApi")));

            services.AddScoped<IGenericRepository<Board>, GenericRepository<Board>>();
            services.AddScoped<IGenericRepository<Card>, GenericRepository<Card>>();

            services.AddScoped<IBoardService, BoardService>();
            services.AddScoped<ICardService, CardService>();

            var mapperConfig = new MapperConfiguration(config => config.AddProfile(new MappingProfile()));
            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddScoped<IValidator<BoardDto>, BoardValidator>();
            services.AddScoped<IValidator<CardDto>, CardValidator>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

