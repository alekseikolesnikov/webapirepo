﻿using Microsoft.AspNetCore.Mvc;
using Services;
using Services.DTO;

namespace WebAPI.Controllers
{
    public class BoardControllerr : Controller
    {
        private readonly BoardService _service;
        public BoardControllerr(BoardService service)
        {
            _service = service;
        }

        public IActionResult Get(int id)
        {
            var model = _service.Read(id);
            return View(model);
        }

        public IActionResult Create(BoardDto dto)
        {
            var model = _service.Create(dto);
            return View(model);
        }

        public IActionResult Update(BoardDto dto)
        {
            var model = _service.Update(dto);
            return View(model);
        }

        public IActionResult Delete(BoardDto dto)
        {
            _service.Delete(dto);
            return View(); //todo
        }
    }
}